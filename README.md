# Fiches-Cours-Calculs-Seconde

Ce projet contient des fiches de cours sur le calcul numérique et algébrique (niveau seconde).

![image](/uploads/c8ba98ac3485039430228024a2f6c07d/image.png)

## License
Ce document est placé sous licence CC-BY 4.0

<img src="https://licensebuttons.net/l/by/3.0/88x31.png">

## Autres remarques
Le document a été créé à l'aide de Libre Office et de l'extension TexMaths. Le fichier a malheureusement tendance à bugguer lors de l'édition au niveau des sauts de pages et des marges.

## Contributions
Ce projet n'est pas ouvert aux contributions extérieurs (mais vous êtes libre de faire un fork).
